use std::fs;

const INPUT_PATH: &str = "../input.txt";

fn read_file(filepath: &str) -> String {
    fs::read_to_string(filepath).expect("Error: Should have been able to read the file")
}

fn split_input(input: &str) -> (Vec<&str>, usize, Vec<&str>) {
    let data_split: Vec<&str> = input.split("\n\n").collect();
    let mut stacks: Vec<&str> = data_split[0].split('\n').collect();
    let size = stacks
        .pop()
        .unwrap()
        .split_whitespace()
        .last()
        .unwrap()
        .parse::<usize>()
        .expect("This should not error");
    let moves = data_split[1].split('\n').collect();

    (stacks, size, moves)
}

fn parse_stacks(input: &[&str], size: usize) -> Vec<Vec<char>> {
    let mut stacks: Vec<Vec<char>> = (0..size).map(|_| Vec::new()).collect();

    // Parse line by grouping chunks of 4 chars
    // for each chunk get the char in the position 1 (the relevant pos)
    // collect into vector of vectors
    // iterate through them backwards (the bottom item of the stack)
    // push ones that aren't spaces, by keeping index, we make sure
    // they end up in the right stack
    input
        .iter()
        .map(|line| {
            line.chars()
                .collect::<Vec<char>>()
                .chunks(4)
                .map(|chunk| chunk[1])
                .collect::<Vec<char>>()
        })
        .collect::<Vec<Vec<char>>>()
        .iter()
        .rev()
        .for_each(|vec| {
            vec.iter().enumerate().for_each(|(i, c)| {
                if *c != ' ' {
                    stacks[i].push(*c)
                }
            })
        });

    println!("Matrix: ");
    stacks.iter().for_each(|v| println!("{:?}", v));
    stacks
}

fn parse_instructions_to_indexes(order: &str) -> (usize, usize, usize) {
    let parsed_vector: Vec<usize> = order
        .split_ascii_whitespace()
        .filter_map(|s| s.parse::<usize>().ok())
        .collect();
    (
        parsed_vector[0] - 1,
        parsed_vector[1] - 1,
        parsed_vector[2] - 1,
    )
}

fn process_crates_part_a(data: &str) -> String {
    let (raw_stacks, size, raw_moves) = split_input(data);

    let mut stacks = parse_stacks(&raw_stacks, size);

    for order in raw_moves {
        println!("{}", order);
        let (count, from, to) = parse_instructions_to_indexes(order);

        for _ in 0..=count {
            let tmp = stacks[from].pop().unwrap();
            stacks[to].push(tmp);
        }

        stacks.iter().for_each(|v| println!("{:?}", v));
    }

    stacks
        .iter()
        .map(|s| String::from(*s.last().unwrap()))
        .collect::<String>()
}

fn process_crates_part_b(data: &str) -> String {
    let (raw_stacks, size, raw_moves) = split_input(data);

    let mut stacks = parse_stacks(&raw_stacks, size);

    for order in raw_moves {
        println!("{}", order);
        let (mut count, from, to) = parse_instructions_to_indexes(order);
        count = count + 1; // in this case we want the count not the index

        let drain_index = stacks[from].len() - count;
        let tmp: Vec<char> = stacks[from].drain((drain_index)..).collect();
        stacks[to] = [stacks[to].clone(), tmp].concat();

        stacks.iter().for_each(|v| println!("{:?}", v));
    }

    stacks
        .iter()
        .map(|s| String::from(*s.last().unwrap()))
        .collect::<String>()
}

fn main() {
    println!("Hello, world!");

    let data = read_file(INPUT_PATH);

    let res_a = process_crates_part_a(&data);
    println!("\nResult Part A: {}", res_a);

    let res_b = process_crates_part_b(&data);
    println!("\nResult Part B: {}", res_b);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "test.txt";

    #[test]
    // #[ignore]
    fn it_runs_part_b() {
        let data = read_file(TEST_INPUT);
        println!("data:\n{}", data);

        let res = process_crates_part_b(&data);
        assert_eq!(res, "MCD");
    }

    #[test]
    #[ignore]
    fn it_runs_part_a() {
        let data = read_file(TEST_INPUT);
        println!("data:\n{}", data);

        let res = process_crates_part_a(&data);
        assert_eq!(res, "CMZ");
    }

    #[test]
    #[ignore]
    fn it_splits_data() {
        let data = read_file(TEST_INPUT);
        let (stacks, size, moves) = split_input(&data);
        println!("data:\n{:?}\n{}\n{:?}", stacks, size, moves);
        assert_eq!(stacks, vec!["    [D]    ", "[N] [C]    ", "[Z] [M] [P]"]);
        assert_eq!(
            moves,
            vec![
                "move 1 from 2 to 1",
                "move 3 from 1 to 3",
                "move 2 from 2 to 1",
                "move 1 from 1 to 2"
            ]
        )
    }

    #[test]
    #[ignore]
    fn it_process_stacks() {
        let data = read_file(TEST_INPUT);
        let (stacks, size, _) = split_input(&data);
        println!("stack:\n {:?}", stacks);
        println!("size:\n {}", size);
        let parsed = parse_stacks(&stacks, size);
        println!("parsed stacks: {:?}", parsed);
    }

    #[test]
    #[ignore]
    fn it_parses_moves() {
        let data = read_file(TEST_INPUT);
        let (_, _, moves) = split_input(&data);
        println!("stack:\n {:?}", moves);

        // let parsed = process_stacks(&stacks, size);
        // println!("parsed stacks: {:?}", parsed);
    }
}
