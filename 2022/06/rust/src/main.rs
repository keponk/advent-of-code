use std::fs;

const INPUT_PATH: &str = "../input.txt";

fn read_file(filepath: &str) -> String {
    fs::read_to_string(filepath).expect("Error: Should have been able to read the file")
}

fn find_marker(seq_size: usize, data: &str) -> usize {
    data.chars()
        .enumerate()
        .map(|(index, c)| (index, c))
        .collect::<Vec<(usize, char)>>()
        .windows(seq_size)
        .find(|window| {
            // println!("Checking {:?} ", window);
            let mut win = window.iter().map(|(_, c)| *c).collect::<Vec<char>>();
            win.sort();
            win.dedup();
            win.len() == seq_size
        })
        .unwrap()
        .last()
        .unwrap()
        .0
        + 1
}

fn main() {
    let data = read_file(INPUT_PATH);

    let res_a = find_marker(4, &data);
    println!("\nResult Part A: {}", res_a);

    let res_b = find_marker(14, &data);
    println!("\nResult Part B: {}", res_b);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "test.txt";

    #[test]
    // #[ignore]
    fn it_runs_part_b() {
        // let data = read_file(&TEST_INPUT);

        let res_a = find_marker(14, "mjqjpqmgbljsphdztnvjfqwrcgsmlb");

        assert_eq!(res_a, 19);
    }

    #[test]
    #[ignore]
    fn it_runs_part_a() {
        let data = read_file(&TEST_INPUT);

        let res_a = find_marker(4, &data);

        assert_eq!(res_a, 7);
    }
}
