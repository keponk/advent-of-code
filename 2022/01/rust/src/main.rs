use std::fs;

const INPUT_PATH: &str = "../input.txt";

pub fn find_most_calories_part_a(data: &[&str]) -> u32 {
    data.iter()
        .map(|elf| {
            elf.split('\n')
                .map(|calorie| calorie.parse::<u32>().unwrap())
                .sum()
        })
        .max()
        .unwrap()
}

pub fn find_top_elves_sum_part_b(data: &[&str]) -> u32 {
    let mut calories: Vec<u32> = data
        .iter()
        .map(|elf| {
            elf.split('\n')
                .map(|calorie| calorie.parse::<u32>().expect("all values should be valis"))
                .sum()
        })
        .collect::<Vec<u32>>();
    calories.sort();
    calories.iter().rev().take(3).sum()
}

fn main() {
    let data =
        fs::read_to_string(INPUT_PATH).expect("Error: Should have been able to read the file");

    let elves: Vec<&str> = data.split("\n\n").collect();
    println!("There are {} elves", elves.len());

    let part_a = find_most_calories_part_a(&elves);
    println!("Answer Part A: {}", part_a);

    let top = find_top_elves_sum_part_b(&elves);
    println!("Answer Part B: {}", top);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    const TEST_FILE: &str = "test.txt";

    #[test]
    fn it_reads_file() {
        let test_input: String =
            fs::read_to_string(TEST_FILE).expect("Error: Should be able to read the file");
        assert_eq!(test_input, "1\n2\n\n3");
    }

    #[test]
    fn it_finds_largest_sum() {
        let test_input: String =
            fs::read_to_string(TEST_FILE).expect("Error: Should be able to read the file");

        let data: Vec<&str> = test_input.split("\n\n").collect();

        let result = find_most_calories_part_a(&data);
        assert_eq!(result, 15)
    }
}
