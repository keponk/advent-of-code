use itertools::Itertools;
use std::{fs, ops::RangeInclusive};

const INPUT_PATH: &str = "../input.txt";

pub fn get_priority(letter: &char) -> u32 {
    let range_to_vec = |vec: RangeInclusive<u8>| vec.map(|v| v as char).collect::<Vec<char>>();

    let priorities: Vec<char> = [range_to_vec(b'a'..=b'z'), range_to_vec(b'A'..=b'Z')].concat();
    priorities.iter().position(|c| c == letter).unwrap() as u32 + 1
}

pub fn find_dupe(line: &str) -> char {
    let (left, right) = line.split_at(line.len() / 2);
    left.chars().find(|c| right.contains(*c)).unwrap()
}

pub fn calculate_prios_part_a(data: &Vec<&str>) -> u32 {
    data.iter()
        .fold(0, |acc, line| get_priority(&find_dupe(line)) + acc)
}

pub fn calculate_groups_part_b(data: &Vec<&str>) -> u32 {
    // iterate per group
    data.iter().chunks(3).into_iter().fold(0u32, |acc, bag| {
        // convert groups to char vecs
        let group: Vec<Vec<char>> = bag
            .into_iter()
            .map(|s| s.chars().dedup().collect())
            .collect();

        // filter get common item
        let group_item = group.iter().skip(1).fold(group[0].clone(), |acc, v| {
            acc.iter().filter(|c| v.contains(c)).map(|c| *c).collect()
        })[0];

        // sum group items
        acc + get_priority(&group_item)
    })
}

fn main() {
    let data =
        fs::read_to_string(INPUT_PATH).expect("Error: Should have been able to read the file");
    let data_split: Vec<&str> = data.split("\n").collect();
    
    let sum_a = calculate_prios_part_a(&data_split);
    println!("Result: {}", sum_a);

    let sum_b = calculate_groups_part_b(&data_split);
    println!("Result: {}", sum_b);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_finds_dupe() {
        let line = "asdfqwef";
        assert_eq!(find_dupe(line), 'f');
    }

    #[test]
    fn it_gets_priorities() {
        let res_low_a = get_priority(&'a');
        assert_eq!(res_low_a, 1);

        let res_upp_a = get_priority(&'A');
        assert_eq!(res_upp_a, 27);
    }
}
