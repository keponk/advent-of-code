use std::{collections::HashSet, fs};

const INPUT_FILE: &str = "../input.txt";

fn read_input_file(input_file: &str) -> String {
    fs::read_to_string(input_file).expect("Error: Should have been able to read the file")
}

fn split_by_comma(input: &str) -> (&str, &str) {
    let data: Vec<&str> = input.split(',').collect();
    (data[0], data[1])
}

fn str_to_hash(input: &str) -> HashSet<u32> {
    let values: Vec<u32> = input
        .split('-')
        .map(|s| s.parse::<u32>().expect("There should be only valid values"))
        .collect();
    HashSet::from_iter(values[0]..=values[1])
}

fn process_data_part_a(data: &str) -> u32 {
    data.split('\n').fold(0, |acc, line| {
        let (left, right) = split_by_comma(line);
        let range1 = str_to_hash(left);
        let range2 = str_to_hash(right);

        if range1.is_subset(&range2) || range2.is_subset(&range1) {
            acc + 1
        } else {
            acc
        }
    })
}

fn process_data_part_b(data: &str) -> u32 {
    data.split('\n').fold(0, |acc, line| {
        let (left, right) = split_by_comma(line);
        let range1 = str_to_hash(left);
        let range2 = str_to_hash(right);

        if range1.intersection(&range2).collect::<Vec<&u32>>().len() > 0 {
            acc + 1
        } else {
            acc
        }
    })
}

fn main() {
    let input = read_input_file(INPUT_FILE);

    let part_a = process_data_part_a(&input);
    println!("Answer part A: {}", part_a);

    let part_b = process_data_part_b(&input);
    println!("Answer part B: {}", part_b);
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "test.txt";

    #[test]
    fn it_runs_part_a() {
        let data = read_input_file(TEST_INPUT);
        println!("data:\n{}", data);
        let res = process_data_part_a(&data);
        assert_eq!(res, 4)
    }
}
