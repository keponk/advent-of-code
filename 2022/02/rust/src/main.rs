use std::fs;

const INPUT_FILE: &str = "../input.txt";

pub fn read_input_file(path: &str) -> String {
    fs::read_to_string(path).expect("Error: Should have been able to read the file")
}

pub fn count_round_part_a(values: &str) -> u32 {
    match values {
        "A X" => 3 + 1,
        "A Y" => 6 + 2,
        "A Z" => 0 + 3,
        "B X" => 0 + 1,
        "B Y" => 3 + 2,
        "B Z" => 6 + 3,
        "C X" => 6 + 1,
        "C Y" => 0 + 2,
        "C Z" => 3 + 3,
        _ => 0,
    }
}

pub fn calculate_points_part_a(data: &str) -> u32 {
    data.split("\n")
        .fold(0, |acc, line| count_round_part_a(line) + acc)
}

pub fn calculate_points_part_b(data: &str) -> u32 {
    data.split("\n").fold(0, |acc, line| 
        match line {
            "A X" => 0 + 3,
            "A Y" => 3 + 1,
            "A Z" => 6 + 2,
            "B X" => 0 + 1,
            "B Y" => 3 + 2,
            "B Z" => 6 + 3,
            "C X" => 0 + 2,
            "C Y" => 3 + 3,
            "C Z" => 6 + 1,
            _ => 0,
        } + acc
    )
}

fn main() {
    let file_data = read_input_file(INPUT_FILE);
    let result = calculate_points_part_a(&file_data);
    println!("Result part A: {}", result);

    let part_b = calculate_points_part_b(&file_data);
    println!("Result par B: {}", part_b);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn it_reads_file() {
        let result = read_input_file("test.txt");
        assert_eq!(result, "A Z\nC Z\nC Z\nA X\nC X\nC Y\nB Y");
    }

    #[test]
    #[ignore]
    fn it_counts_game_part_a() {
        let result_one = count_round_part_a("A Y");
        assert_eq!(result_one, 8);

        let result_two = count_round_part_a("B X");
        assert_eq!(result_two, 1);

        let result_three = count_round_part_a("C Z");

        assert_eq!(result_three, 6);
    }

    #[test]
    #[ignore]
    fn it_calcs_all_points_part_a() {
        let data = read_input_file("test.txt");
        let result = calculate_points_part_a(&data);
        assert_eq!(result, 33)
    }
}
