use std::fs;

use phf::phf_map;

const INPUT_PATH: &str = "../input.txt";
// const INPUT_PATH: &str = "../sample.txt";

static NUMBERS: phf::Map<&'static str, u32> = phf_map! {
    "one" => 1,
    "two" => 2,
    "three" => 3,
    "four" => 4,
    "five" => 5,
    "six" => 6,
    "seven" => 7,
    "eight" => 8,
    "nine" => 9,
};

fn read_file(filepath: &str) -> String {
    fs::read_to_string(filepath).expect("Error: Should have been able to read the file")
}

fn get_pair(text: &str) -> u32 {
    let nums: Vec<u32> = text
        .matches(char::is_numeric)
        .map(|s| s.parse().unwrap())
        .collect();

    println!("nums: {:?}", nums);
    return (nums[0] * 10) + nums[nums.len() - 1];
}

fn part_two(text: &str) -> u32 {
    println!("before: {}", &text);

    let mut idxs: Vec<(usize, u32)> = Vec::new();
    NUMBERS.entries().for_each(|(k, v)| {
        text.to_ascii_lowercase()
            .match_indices(k)
            .for_each(|(i, _)| {
                idxs.push((i, *v));
            });
    });
    idxs.sort_by(|a, b| a.0.cmp(&b.0));

    println!("idxs: {:?}", idxs);

    let fi = text.find(char::is_numeric);
    let la = text.rfind(char::is_numeric);
    println!("fi/la: {:?} {:?}", fi, la);

    let mut offset = 0;
    let mut new_text: String = text.to_string().to_ascii_lowercase();
    if idxs.len() > 0 {
        match (fi, la) {
            (None, None) => {
                idxs.iter().for_each(|(i, v)| {
                    let (left, right) = new_text.split_at(i + offset);
                    new_text = format!("{}{}{}", left, v, right);
                    offset += 1;
                });
            }
            (_, _) => {
                if let Some(i) = fi {
                    if i > idxs[0].0 {
                        let (left, right) = new_text.split_at(idxs[0].0 + offset);
                        new_text = format!("{}{}{}", left, idxs[0].1, right);
                        offset += 1;
                    }
                }

                if let Some(i) = la {
                    if i < idxs[idxs.len() - 1].0 {
                        let (left, right) = new_text.split_at(idxs[idxs.len() - 1].0 + offset);
                        new_text = format!("{}{}{}", left, idxs[idxs.len() - 1].1, right);
                    }
                }
            }
        }
    }

    println!("after: {}", &new_text);

    return get_pair(&new_text);
}

fn main() {
    let data = read_file(INPUT_PATH);

    // println!("{}", &data);

    // let res_one: u32 = data
    //     .split_ascii_whitespace()
    //     .map(|i: &str| get_pair(i))
    //     .sum();
    //
    // println!("{}", res_one);

    let res_two: u32 = data
        .split_ascii_whitespace()
        .map(|i: &str| part_two(i))
        .sum();

    println!("{}", res_two);
    ()
}
